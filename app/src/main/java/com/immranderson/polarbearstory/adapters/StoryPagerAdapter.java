package com.immranderson.polarbearstory.adapters;

import android.support.v4.app.FragmentManager;

import com.immranderson.polarbearstory.fragments.BaseFragment;
import com.immranderson.polarbearstory.fragments.IntroFragment;
import com.immranderson.polarbearstory.fragments.Page10Fragment;
import com.immranderson.polarbearstory.fragments.Page11Fragment;
import com.immranderson.polarbearstory.fragments.Page12Fragment;
import com.immranderson.polarbearstory.fragments.Page13Fragment;
import com.immranderson.polarbearstory.fragments.Page14Fragment;
import com.immranderson.polarbearstory.fragments.Page15Fragment;
import com.immranderson.polarbearstory.fragments.Page16Fragment;
import com.immranderson.polarbearstory.fragments.Page17Fragment;
import com.immranderson.polarbearstory.fragments.Page18Fragment;
import com.immranderson.polarbearstory.fragments.Page19Fragment;
import com.immranderson.polarbearstory.fragments.Page1Fragment;
import com.immranderson.polarbearstory.fragments.Page20Fragment;
import com.immranderson.polarbearstory.fragments.Page21Fragment;
import com.immranderson.polarbearstory.fragments.Page22Fragment;
import com.immranderson.polarbearstory.fragments.Page23Fragment;
import com.immranderson.polarbearstory.fragments.Page24Fragment;
import com.immranderson.polarbearstory.fragments.Page25Fragment;
import com.immranderson.polarbearstory.fragments.Page26Fragment;
import com.immranderson.polarbearstory.fragments.Page27Fragment;
import com.immranderson.polarbearstory.fragments.Page28Fragment;
import com.immranderson.polarbearstory.fragments.Page29Fragment;
import com.immranderson.polarbearstory.fragments.Page2Fragment;
import com.immranderson.polarbearstory.fragments.Page30Fragment;
import com.immranderson.polarbearstory.fragments.Page31Fragment;
import com.immranderson.polarbearstory.fragments.Page32Fragment;
import com.immranderson.polarbearstory.fragments.Page33Fragment;
import com.immranderson.polarbearstory.fragments.Page34Fragment;
import com.immranderson.polarbearstory.fragments.Page35Fragment;
import com.immranderson.polarbearstory.fragments.Page3Fragment;
import com.immranderson.polarbearstory.fragments.Page4Fragment;
import com.immranderson.polarbearstory.fragments.Page5Fragment;
import com.immranderson.polarbearstory.fragments.Page6Fragment;
import com.immranderson.polarbearstory.fragments.Page7Fragment;
import com.immranderson.polarbearstory.fragments.Page8Fragment;
import com.immranderson.polarbearstory.fragments.Page9Fragment;

public class StoryPagerAdapter extends BaseStatePagerAdapter {


    public StoryPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public BaseFragment getItem(int position) {

        switch (position) {

            case 0:

                return IntroFragment.newInstance();

            case 1:

                return Page1Fragment.newInstance();

            case 2:

                return Page2Fragment.newInstance();

            case 3:

                return Page3Fragment.newInstance();

            case 4:

                return Page4Fragment.newInstance();

            case 5:

                return Page5Fragment.newInstance();

            case 6:

                return Page6Fragment.newInstance();

            case 7:

                return Page7Fragment.newInstance();

            case 8:

                return Page8Fragment.newInstance();

            case 9:

                return Page9Fragment.newInstance();

            case 10:

                return Page10Fragment.newInstance();

            case 11:

                return Page11Fragment.newInstance();

            case 12:

                return Page12Fragment.newInstance();

            case 13:

                return Page13Fragment.newInstance();

            case 14:

                return Page14Fragment.newInstance();

            case 15:

                return Page15Fragment.newInstance();


            case 16:

                return Page16Fragment.newInstance();

            case 17:

                return Page17Fragment.newInstance();

            case 18:

                return Page18Fragment.newInstance();
            case 19:

                return Page19Fragment.newInstance();
            case 20:

                return Page20Fragment.newInstance();
            case 21:

                return Page21Fragment.newInstance();
            case 22:

                return Page22Fragment.newInstance();
            case 23:

                return Page23Fragment.newInstance();
            case 24:

                return Page24Fragment.newInstance();
            case 25:

                return Page25Fragment.newInstance();
            case 26:

                return Page26Fragment.newInstance();
            case 27:

                return Page27Fragment.newInstance();
            case 28:

                return Page28Fragment.newInstance();
            case 29:

                return Page29Fragment.newInstance();
            case 30:

                return Page30Fragment.newInstance();
            case 31:

                return Page31Fragment.newInstance();
            case 32:

                return Page32Fragment.newInstance();
            case 33:

                return Page33Fragment.newInstance();
            case 34:

                return Page34Fragment.newInstance();

            case 35:

                return Page35Fragment.newInstance();


            default:

                return IntroFragment.newInstance();

        }

    }

    @Override
    public int getCount() {
        return 36;
    }

}
