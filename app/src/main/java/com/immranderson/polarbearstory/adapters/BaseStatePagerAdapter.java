package com.immranderson.polarbearstory.adapters;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.immranderson.polarbearstory.fragments.BaseFragment;

public abstract class BaseStatePagerAdapter extends FragmentStatePagerAdapter {

    private SparseArray<BaseFragment> registeredFragments = new SparseArray<>();

    public BaseStatePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        BaseFragment fragment = (BaseFragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public BaseFragment getFragmentAtPosition(int position) {
        return registeredFragments.get(position);
    }
}
