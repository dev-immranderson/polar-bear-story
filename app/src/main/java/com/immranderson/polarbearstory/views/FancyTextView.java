package com.immranderson.polarbearstory.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class FancyTextView extends TextView {

    public FancyTextView(Context context) {
        super(context);
        init();
    }

    public FancyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FancyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public FancyTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init() {

        Typeface type = Typeface.createFromAsset(getContext().getAssets(), "fonts/tangerine_bold.ttf");
        setTypeface(type);

    }
}
