package com.immranderson.polarbearstory.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.immranderson.polarbearstory.R;

public class MaskedImageView extends ImageView {

    private Bitmap mImage;
    private Bitmap mMask; // png mask with transparency
    private final int mPosX = 0;
    private final int mPosY = 0;

    private final Paint maskPaint;
    private final Paint imagePaint;


    public MaskedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        maskPaint = new Paint();
        maskPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        imagePaint = new Paint();
        imagePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OVER));

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MaskedImageView, 0, 0);

        try {

//            mShowText = a.getBoolean(R.styleable.PieChart_showText, false);
//            mTextPos = a.getInteger(R.styleable.PieChart_labelPosition, 0);


            Drawable drawable = a.getDrawable(R.styleable.MaskedImageView_maskSrc);

            mMask = ((BitmapDrawable) drawable).getBitmap();
            mImage = BitmapFactory.decodeResource(getResources(), attrs.getAttributeResourceValue("http://schemas.android.com/apk/res/android", "src", 0));


        } finally {
            a.recycle();
        }

    }


//    private void init() {
//
//
//
//    }

//    public MaskedImageView (final Context context) {
//        super();

//    }

/* TODO
 if you have more constructors, make sure you initialize maskPaint and imagePaint
 Declaring these as final means that all your constructors have to initialize them.
 Failure to do so = your code won't compile.
*/

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.save();
        canvas.drawBitmap(mMask, 0, 0, maskPaint);
        canvas.drawBitmap(mImage, mPosX, mPosY, imagePaint);
        canvas.restore();

    }

}
