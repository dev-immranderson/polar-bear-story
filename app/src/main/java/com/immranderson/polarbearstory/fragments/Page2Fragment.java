package com.immranderson.polarbearstory.fragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.immranderson.polarbearstory.R;
import com.immranderson.polarbearstory.databinding.FragmentPage2Binding;

public class Page2Fragment extends BaseFragment {

    private FragmentPage2Binding b;

    public static Page2Fragment newInstance() {

        Bundle args = new Bundle();

        Page2Fragment fragment = new Page2Fragment();
        fragment.setArguments(args);
        return fragment;

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_page_2, container, false);

        b.heartImageView.setOnClickListener(v -> {

            Animator first = getZoomAnimator(v, 0.8f);
            Animator second = getZoomAnimator(v, 1.2f);
            Animator third = getZoomAnimator(v, 0.9f);
            Animator fourth = getZoomAnimator(v, 1.0f);

            AnimatorSet heartBeatSet = new AnimatorSet();

            heartBeatSet.playSequentially(first, second, third, fourth);

            heartBeatSet.start();

            pSoundListener.onSoundEffectPlayed(R.raw.yeeees);

        });


        return b.getRoot();

    }

}
