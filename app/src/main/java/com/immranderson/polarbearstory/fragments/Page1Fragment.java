package com.immranderson.polarbearstory.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.immranderson.polarbearstory.R;
import com.immranderson.polarbearstory.databinding.FragmentPage1Binding;

public class Page1Fragment extends BaseFragment {

    private FragmentPage1Binding b;

    public static Page1Fragment newInstance() {

        Bundle args = new Bundle();

        Page1Fragment fragment = new Page1Fragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialogRes = R.raw.p1;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_page_1, container, false);

        pTextViews.add(b.onceUponATime);
        pTextViews.add(b.thereWasALittleGirl);
        pTextViews.add(b.whoLovedAPolarBear);

        initTextViewsToInvisible();


//        b.introPictureImageView.setOnClickListener(v -> {
//
//            pSoundListener.onSoundEffectPlayed(R.raw.tommy_bear);
//            animateWiggle(v);
//
//        });
//
//        b.introTextView.setOnClickListener(v -> {
//
//            pSoundListener.onReadAloudPlayed(R.raw.once_upon_a_time);
//
//        });

        return b.getRoot();

    }
}
