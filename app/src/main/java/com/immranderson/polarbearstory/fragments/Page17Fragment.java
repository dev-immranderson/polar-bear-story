package com.immranderson.polarbearstory.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.immranderson.polarbearstory.R;
import com.immranderson.polarbearstory.databinding.FragmentPage17Binding;

public class Page17Fragment extends BaseFragment {

    private FragmentPage17Binding b;

    public static Page17Fragment newInstance() {

        Bundle args = new Bundle();

        Page17Fragment fragment = new Page17Fragment();
        fragment.setArguments(args);
        return fragment;

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialogRes = R.raw.p17;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_page_17, container, false);

        pTextViews.add(b.introTextView);
        initTextViewsToInvisible();

        return b.getRoot();

    }
}
