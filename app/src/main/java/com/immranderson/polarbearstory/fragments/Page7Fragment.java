package com.immranderson.polarbearstory.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.immranderson.polarbearstory.R;
import com.immranderson.polarbearstory.databinding.FragmentPage7Binding;

public class Page7Fragment extends BaseFragment {

    private FragmentPage7Binding b;

    public static Page7Fragment newInstance() {

        Bundle args = new Bundle();

        Page7Fragment fragment = new Page7Fragment();
        fragment.setArguments(args);
        return fragment;

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_page_7, container, false);

        b.introPictureImageView.setOnClickListener(v -> {

            pSoundListener.onSoundEffectPlayed(R.raw.tadaaaa);
            animateWiggle(v);

        });

        return b.getRoot();

    }
}
