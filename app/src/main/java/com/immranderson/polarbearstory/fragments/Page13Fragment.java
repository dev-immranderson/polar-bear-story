package com.immranderson.polarbearstory.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.immranderson.polarbearstory.R;
import com.immranderson.polarbearstory.databinding.FragmentPage13Binding;

public class Page13Fragment extends BaseFragment {

    private FragmentPage13Binding b;

    public static Page13Fragment newInstance() {

        Bundle args = new Bundle();

        Page13Fragment fragment = new Page13Fragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialogRes = R.raw.p13;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_page_13, container, false);

        pTextViews.add(b.introTextView5);
        initTextViewsToInvisible();

        return b.getRoot();

    }
}
