package com.immranderson.polarbearstory.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.immranderson.polarbearstory.R;
import com.immranderson.polarbearstory.databinding.FragmentPage10Binding;

public class Page10Fragment extends BaseFragment {

    private FragmentPage10Binding b;

    public static Page10Fragment newInstance() {

        Bundle args = new Bundle();

        Page10Fragment fragment = new Page10Fragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialogRes = R.raw.p10;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_page_10, container, false);

        pTextViews.add(b.introTextView);
        pTextViews.add(b.introTextView2);
        pTextViews.add(b.introTextView3);


        initTextViewsToInvisible();

        return b.getRoot();

    }
}
