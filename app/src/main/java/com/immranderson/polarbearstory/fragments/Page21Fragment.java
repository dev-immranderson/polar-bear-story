package com.immranderson.polarbearstory.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.immranderson.polarbearstory.R;
import com.immranderson.polarbearstory.databinding.FragmentPage21Binding;

public class Page21Fragment extends BaseFragment {

    private FragmentPage21Binding b;

    public static Page21Fragment newInstance() {

        Bundle args = new Bundle();

        Page21Fragment fragment = new Page21Fragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialogRes = R.raw.p21;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_page_21, container, false);

        pTextViews.add(b.introTextView);
        pTextViews.add(b.introTextView2);
        initTextViewsToInvisible();

        return b.getRoot();

    }
}
