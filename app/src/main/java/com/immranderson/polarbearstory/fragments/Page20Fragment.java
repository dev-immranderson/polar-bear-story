package com.immranderson.polarbearstory.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.immranderson.polarbearstory.R;
import com.immranderson.polarbearstory.databinding.FragmentPage20Binding;

public class Page20Fragment extends BaseFragment {

    private FragmentPage20Binding b;

    public static Page20Fragment newInstance() {

        Bundle args = new Bundle();

        Page20Fragment fragment = new Page20Fragment();
        fragment.setArguments(args);
        return fragment;

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialogRes = R.raw.p20;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_page_20, container, false);

        pTextViews.add(b.introTextView2);
        pTextViews.add(b.introTextView3);
        initTextViewsToInvisible();


        return b.getRoot();

    }
}
