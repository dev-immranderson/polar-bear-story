package com.immranderson.polarbearstory.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.immranderson.polarbearstory.R;
import com.immranderson.polarbearstory.databinding.FragmentPage29Binding;

public class Page29Fragment extends BaseFragment {

    private FragmentPage29Binding b;

    public static Page29Fragment newInstance() {

        Bundle args = new Bundle();

        Page29Fragment fragment = new Page29Fragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialogRes = R.raw.p29;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_page_29, container, false);

        pTextViews.add(b.introTextView);
        pTextViews.add(b.introTextView2);

        initTextViewsToInvisible();

        return b.getRoot();

    }
}
