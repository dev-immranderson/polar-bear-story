package com.immranderson.polarbearstory.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.immranderson.polarbearstory.R;
import com.immranderson.polarbearstory.databinding.FragmentPage12Binding;

public class Page12Fragment extends BaseFragment {

    private FragmentPage12Binding b;

    public static Page12Fragment newInstance() {

        Bundle args = new Bundle();

        Page12Fragment fragment = new Page12Fragment();
        fragment.setArguments(args);
        return fragment;

    }




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_page_12, container, false);


        return b.getRoot();

    }
}
