package com.immranderson.polarbearstory.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.immranderson.polarbearstory.R;
import com.immranderson.polarbearstory.databinding.FragmentPage26Binding;

public class Page26Fragment extends BaseFragment {

    private FragmentPage26Binding b;

    public static Page26Fragment newInstance() {

        Bundle args = new Bundle();

        Page26Fragment fragment = new Page26Fragment();
        fragment.setArguments(args);
        return fragment;

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_page_26, container, false);

        b.introPictureImageView.setOnClickListener(v -> {



        });

        return b.getRoot();

    }
}
