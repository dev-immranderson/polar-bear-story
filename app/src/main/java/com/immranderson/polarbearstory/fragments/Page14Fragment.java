package com.immranderson.polarbearstory.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.immranderson.polarbearstory.R;
import com.immranderson.polarbearstory.databinding.FragmentPage14Binding;

public class Page14Fragment extends BaseFragment {

    private FragmentPage14Binding b;

    public static Page14Fragment newInstance() {

        Bundle args = new Bundle();

        Page14Fragment fragment = new Page14Fragment();
        fragment.setArguments(args);
        return fragment;

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialogRes = R.raw.p14;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_page_14, container, false);

        pTextViews.add(b.introTextView5);
        initTextViewsToInvisible();

        return b.getRoot();

    }
}
