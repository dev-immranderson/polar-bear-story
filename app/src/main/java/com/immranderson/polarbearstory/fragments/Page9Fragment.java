package com.immranderson.polarbearstory.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.immranderson.polarbearstory.R;
import com.immranderson.polarbearstory.databinding.FragmentPage9Binding;

public class Page9Fragment extends BaseFragment {

    private FragmentPage9Binding b;

    public static Page9Fragment newInstance() {

        Bundle args = new Bundle();

        Page9Fragment fragment = new Page9Fragment();
        fragment.setArguments(args);
        return fragment;

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_page_9, container, false);


        return b.getRoot();

    }
}
