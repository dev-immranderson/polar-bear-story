package com.immranderson.polarbearstory.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.immranderson.polarbearstory.R;
import com.immranderson.polarbearstory.databinding.FragmentIntroBinding;

public class IntroFragment extends BaseFragment {

    private FragmentIntroBinding b;

    public static IntroFragment newInstance() {

        Bundle args = new Bundle();

        IntroFragment fragment = new IntroFragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        pDialogRes = R.raw.once_upon_a_time;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_intro, container, false);


        initTextViewsToInvisible();

        new Handler().postDelayed(() -> {

            animateInTextViews();
//            pSoundListener.onReadAloudPlayed(pDialogRes);

        }, 2000);

//        b.introPictureImageView.setOnClickListener(v -> {
//
//            pSoundListener.onSoundEffectPlayed(R.raw.tommy_bear);
//            animateWiggle(v);
//
//        });
//
//        b.introTextView.setOnClickListener(v -> {
//
//            pSoundListener.onReadAloudPlayed(R.raw.once_upon_a_time);
//
//        });

        return b.getRoot();

    }
}
