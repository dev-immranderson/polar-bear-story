package com.immranderson.polarbearstory.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.immranderson.polarbearstory.R;
import com.immranderson.polarbearstory.databinding.FragmentPage3Binding;

public class Page3Fragment extends BaseFragment {

    private FragmentPage3Binding b;

    public static Page3Fragment newInstance() {

        Bundle args = new Bundle();

        Page3Fragment fragment = new Page3Fragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        pDialogRes = R.raw.p3;

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_page_3, container, false);

        pTextViews.add(b.introTextView);

        initTextViewsToInvisible();

        return b.getRoot();

    }
}
