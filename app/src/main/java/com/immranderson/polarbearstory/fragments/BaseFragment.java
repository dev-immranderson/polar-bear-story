package com.immranderson.polarbearstory.fragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.annotation.RawRes;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import com.immranderson.polarbearstory.interfaces.SoundPlayListener;

import java.util.ArrayList;
import java.util.List;

public class BaseFragment extends Fragment {

    protected SoundPlayListener pSoundListener;
    protected List<TextView> pTextViews = new ArrayList<>();
    protected @RawRes int pDialogRes = -1;

    private static final int ANIMATION_TIME = 1000;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        pSoundListener = (SoundPlayListener) context;

    }


    public void initTextViewsToInvisible() {

        for (TextView textView : pTextViews) {

            textView.setAlpha(0f);

        }

    }

    public void animateInTextViews() {

        List<Animator> animators = new ArrayList<>();

        for (int i = 0; i < pTextViews.size(); i++) {

            TextView textView = pTextViews.get(i);
            textView.setAlpha(0f);

            if (i % 2 == 0) {

                animators.add(getFadeInLeftAnimator(textView));

            } else {

                animators.add(getFadeInRightAnimator(textView));

            }

        }

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playSequentially(animators);
        animatorSet.start();

    }

    private Animator getFadeInLeftAnimator(View v) {

        AnimatorSet fadeInLeftAnimator = new AnimatorSet();

        ObjectAnimator leftAnimator = ObjectAnimator.ofFloat(v, View.TRANSLATION_X, -200f, 0f);
        ObjectAnimator fadeInAnimator = ObjectAnimator.ofFloat(v, View.ALPHA, 0f, 1f);
        fadeInLeftAnimator.setDuration(ANIMATION_TIME);
        fadeInLeftAnimator.playTogether(leftAnimator, fadeInAnimator);

        return fadeInLeftAnimator;

    }

    private Animator getFadeInRightAnimator(View v) {


        AnimatorSet fadeInRightAnimator = new AnimatorSet();

        ObjectAnimator rightAnimator = ObjectAnimator.ofFloat(v, TextView.TRANSLATION_X, 200f, 0f);
        ObjectAnimator fadeInAnimator = ObjectAnimator.ofFloat(v, TextView.ALPHA, 0f, 1f);
        fadeInRightAnimator.setDuration(ANIMATION_TIME);
        fadeInRightAnimator.playTogether(rightAnimator, fadeInAnimator);

        return fadeInRightAnimator;

    }

    public void animateWiggle(final View v) {

        AnimatorSet wiggleSet = new AnimatorSet();
        int numWiggles = 6;
        List<Animator> wiggleAnimators = new ArrayList<>(numWiggles);

        for (int i = 0; i <= numWiggles; i ++) {

            if (i == numWiggles) { //final wiggle

                Animator zeroAnimator = ObjectAnimator.ofFloat(v, View.ROTATION, 0f);
                wiggleAnimators.add(zeroAnimator);

            } else {

                if (i % 2 == 0) {

                    Animator positiveAnimator = ObjectAnimator.ofFloat(v, View.ROTATION, 5f);
                    wiggleAnimators.add(positiveAnimator);

                } else {

                    Animator negativeAnimator = ObjectAnimator.ofFloat(v, View.ROTATION, -5f);
                    wiggleAnimators.add(negativeAnimator);

                }

            }

        }

        wiggleSet.playSequentially(wiggleAnimators);
        wiggleSet.setDuration(125);
        wiggleSet.start();


    }


    public Animator getZoomAnimator(View v, float zoomLevel) {

        Animator scaleX = ObjectAnimator.ofFloat(v, View.SCALE_X, zoomLevel);
        Animator scaleY = ObjectAnimator.ofFloat(v, View.SCALE_Y, zoomLevel);

        AnimatorSet totalScale = new AnimatorSet();
        totalScale.playTogether(scaleX, scaleY);
        totalScale.setInterpolator(new AccelerateDecelerateInterpolator());

        return totalScale;

    }

    public void playDialog() {

        if (pDialogRes != -1) {

            pSoundListener.onReadAloudPlayed(pDialogRes);

        }

    }


}
