package com.immranderson.polarbearstory.activities;

import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RawRes;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.immranderson.polarbearstory.R;
import com.immranderson.polarbearstory.ZoomOutPageTransformer;
import com.immranderson.polarbearstory.adapters.StoryPagerAdapter;
import com.immranderson.polarbearstory.databinding.ActivityMainBinding;
import com.immranderson.polarbearstory.fragments.BaseFragment;
import com.immranderson.polarbearstory.interfaces.SoundPlayListener;

public class MainActivity extends AppCompatActivity implements SoundPlayListener {

    private ActivityMainBinding b;
    private StoryPagerAdapter mAdapter;
    private MediaPlayer mSoundEffectPlayer;
    private MediaPlayer mReadAloudPlayer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        b = DataBindingUtil.setContentView(this, R.layout.activity_main);

        mAdapter = new StoryPagerAdapter(getSupportFragmentManager());
        b.mainViewPager.setAdapter(mAdapter);

        b.mainViewPager.setPageTransformer(false, new ZoomOutPageTransformer());

        b.mainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                BaseFragment currentlySelectedFragment = mAdapter.getFragmentAtPosition(position);

                currentlySelectedFragment.animateInTextViews();

                if (mReadAloudPlayer != null) {

                    mReadAloudPlayer.stop();

                }

                currentlySelectedFragment.playDialog();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void onSoundEffectPlayed(@RawRes int soundResource) {

        if (mSoundEffectPlayer != null) {
            mSoundEffectPlayer.stop();
        }

        mSoundEffectPlayer = MediaPlayer.create(this, soundResource);
        mSoundEffectPlayer.start();

    }

    @Override
    public void onReadAloudPlayed(@RawRes int soundResource) {

        if (mReadAloudPlayer != null) {
            mReadAloudPlayer.stop();
        }

        mReadAloudPlayer = MediaPlayer.create(this, soundResource);
        mReadAloudPlayer.start();

    }

}
