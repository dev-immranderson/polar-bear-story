package com.immranderson.polarbearstory.interfaces;

import android.support.annotation.RawRes;

public interface SoundPlayListener {

    void onSoundEffectPlayed(@RawRes int soundResource);
    void onReadAloudPlayed(@RawRes int soundResource);

}
